# Steps
1) Generate basic file that will build the project
2) implement node cache
3) Get/Generate token `surge token`
4) Add token to CI vars 
5) deploy to surge
6) cleanup and optimization
7) show how to make feature builds



# Links
* [surge](https://surge.sh/)
* [surge deployment with token code](https://github.com/yavisht/deploy-via-surge.sh-github-action-template)
* [ci cache](https://docs.gitlab.com/ee/ci/caching/)
* [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)