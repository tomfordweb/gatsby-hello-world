import React from "react"
import PageWrapper from "../components/page-wrapper"

export default function Home() {
  return (
    <PageWrapper>
      <h1>Welcome to my website</h1>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lacus
        metus, tincidunt vel libero at, volutpat luctus arcu. Maecenas eget
        sollicitudin risus. Curabitur efficitur, odio vitae egestas posuere,
        metus est blandit velit, vitae molestie dolor mi vitae massa. Ut finibus
        augue eu lorem molestie, posuere feugiat lectus volutpat. Interdum et
        malesuada fames ac ante ipsum primis in faucibus. Vestibulum eget
        egestas mauris. Integer maximus ligula eu lectus cursus sagittis. Sed mi
        risus, suscipit in finibus a, eleifend sed justo. Phasellus tempus,
        purus eu consequat ultrices, augue eros bibendum nunc, quis dignissim
        sem dui nec ipsum. Curabitur scelerisque odio lobortis, pretium tortor
        et, auctor diam. Vestibulum a congue erat.
      </p>

      <p>
        raesent purus augue, elementum ac dui quis, varius ultrices nulla.
        Nullam odio enim, iaculis vitae sem id, tempor venenatis nulla. In non
        felis dui. Morbi ac mattis urna. Lorem ipsum dolor sit amet, consectetur
        adipiscing elit. In hac habitasse platea dictumst. Sed malesuada felis
        eget bibendum condimentum. Nunc elit dui, luctus euismod ultrices id,
        rhoncus in tellus. Suspendisse ultrices eleifend risus, eget bibendum
        lectus consequat a. Vestibulum aliquam, diam blandit interdum
        sollicitudin, metus tellus bibendum erat, sed congue enim libero eu
        lorem. Quisque dictum quam sit amet mi laoreet vestibulum. Nullam tempus
        ullamcorper tempus. Nullam nunc ligula, venenatis eu ornare ac,
        hendrerit id purus. Donec elementum orci at nisl facilisis scelerisque.
        Pellentesque euismod, ligula et interdum pulvinar, neque tellus pretium
        sapien, sit amet ultricies mi metus auctor justo.
      </p>
    </PageWrapper>
  )
}
