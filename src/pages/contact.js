import React from "react"
import ContactForm from "../components/contact-form"
import PageWrapper from "../components/page-wrapper"

export default function Contact() {
  return (
    <PageWrapper>
      <h1>Contact</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lacus
        metus, tincidunt vel libero at, volutpat luctus arcu. Maecenas eget
        sollicitudin risus. Curabitur efficitur, odio vitae egestas posuere,
        metus est blandit velit, vitae molestie dolor mi vitae massa. Ut finibus
        augue eu lorem molestie, posuere feugiat lectus volutpat. Interdum et
        malesuada fames ac ante ipsum primis in faucibus. Vestibulum eget
        egestas mauris. Integer maximus ligula eu lectus cursus sagittis. Sed mi
        risus, suscipit in finibus a, eleifend sed justo. Phasellus tempus,
        purus eu consequat ultrices, augue eros bibendum nunc, quis dignissim
        sem dui nec ipsum. Curabitur scelerisque odio lobortis, pretium tortor
        et, auctor diam. Vestibulum a congue erat.
      </p>
      <ContactForm />
    </PageWrapper>
  )
}
