import { Link } from "gatsby"
import React from "react"
import "./page-wrapper.css"

export default function PageWrapper({ children }) {
  return (
    <main>
      <header>
        <h4>Hello World</h4>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about/">About</Link>
            </li>
            <li>
              <Link to="/contact/">Contact</Link>
            </li>
          </ul>
        </nav>
      </header>
      <section>{children}</section>
    </main>
  )
}
