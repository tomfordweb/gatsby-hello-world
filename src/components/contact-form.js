import React from "react"

const ContactForm = () => {
  return (
    <article className="contact-form">
      <div class="form-control">
        <input type="text" name="name" placeholder="Name" />
      </div>
      <div class="form-control">
        <input type="text" name="email" placeholder="Email" />
      </div>
      <div class="form-control">
        <textarea name="message" placeholder="Message"></textarea>
      </div>

      <div class="form-contorl">
        <input type="submit" value="Submit" />
      </div>
    </article>
  )
}

export default ContactForm
